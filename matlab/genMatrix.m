nr = 4000;
nc = 6000;
k = 100;
thresh = .97;
filename = ['../data/lr-' num2str(nr) '-' num2str(nc) '.txt'];
filename2 = ['../data/lr-' num2str(nr) '-' num2str(nc) '.binary.txt'];

disp(filename)

Af = randn(nr,nc);
[U S V] = svd(Af);
Alr = U(:,1:k)*V(:,1:k)' + 1;


take = rand(nr,nc) > thresh;

fileID = fopen(filename,'w');
fileID2 = fopen(filename2,'w');

logit = @(x) 1/(1 + exp(-x))
for i = 1:nr
    for j = 1:nc
       if(rand > 0.5 && logit(Alr(i,j)) > 0.7)
           fprintf(fileID2,'%d %d %f\n',i,j,1.0);
       end
        
       if(take(i,j))
           fprintf(fileID,'%d %d %f\n',i,j,Alr(i,j));
       end
    end
end
