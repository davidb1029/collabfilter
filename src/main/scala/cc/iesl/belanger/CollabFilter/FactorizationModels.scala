package cc.iesl.belanger.CollabFilter

import cc.factorie.la.{SingletonBinaryTensor1, WeightsMapAccumulator, DenseTensor1}
import cc.factorie.{Weights1, Parameters}
import cc.factorie.util.DoubleAccumulator

trait FactorizationModel extends Parameters {
  def initializeWeights: Unit
  def accumulateValueAndGradient(cell: Cell,decomp: DecompSpec, value: DoubleAccumulator, gradient: WeightsMapAccumulator): Unit
  def linearPredict(cell: Cell): Double
  def columnSimilarity(c1: Int, c2: Int): Double
}

class UVModel(matrixSpec: CollabMatrix) extends FactorizationModel{
  //users are rows and items are columns
  //in the tensors, we store the transpose of U and V, though. This is because we can grab columns of U and V efficiently using matrix multiply by a singleton binary tensor
  private val userWeights = (0 until matrixSpec.numRows).map(i => Weights(new DenseTensor1(matrixSpec.rank)))
  private val itemWeights = (0 until matrixSpec.numCols).map(i => Weights(new DenseTensor1(matrixSpec.rank)))
  private val userBiases =  (0 until matrixSpec.numRows).map(i => Weights(new DenseTensor1(1)))
  private val itemBiases =  (0 until matrixSpec.numCols).map(i => Weights(new DenseTensor1(1)))
  val globalBias =  Weights(new DenseTensor1(1))

  def getUserWeights(cell: Cell): Weights1 = userWeights(cell.iCoord)
  def getItemWeights(cell: Cell): Weights1 = itemWeights(cell.jCoord)
  def getUserBias(cell: Cell): Weights1 = userBiases(cell.iCoord)
  def getItemBias(cell: Cell): Weights1 = itemBiases(cell.jCoord)

  def linearPredict(cell: Cell) = (getUserWeights(cell).value dot getItemWeights(cell).value) + getUserBias(cell).value.asArray(0) + getItemBias(cell).value.asArray(0) +  globalBias.value.asArray(0)
  val dummySingletonTensor = new SingletonBinaryTensor1(1,0)

  def accumulateValueAndGradient(cell: Cell,decomp: DecompSpec ,value: DoubleAccumulator, gradient: WeightsMapAccumulator): Unit =  {
    val thisUsersWeights = getUserWeights(cell)
    val thisItemsWeights = getItemWeights(cell)

    val dotProd = linearPredict(cell)

    val(objective,lossGradient) = decomp.lossAndGradient(dotProd,cell.value)

    if (value ne null) value.accumulate(objective) //inside the objective computation above, there's a multiply by -1 because factorie maximizes instead of minimizes

    if (gradient ne null) {
      gradient.accumulate(thisUsersWeights, thisItemsWeights.value,lossGradient)
      gradient.accumulate(thisItemsWeights, thisUsersWeights.value,lossGradient)
      gradient.accumulate(getUserBias(cell),dummySingletonTensor,lossGradient)
      gradient.accumulate(getItemBias(cell),dummySingletonTensor,lossGradient)
      gradient.accumulate(globalBias,dummySingletonTensor,lossGradient)
    }
  }

  def columnSimilarity(c1: Int, c2: Int) = itemWeights(c1).value.cosineSimilarity(itemWeights(c2).value)
  def initializeWeights = {
    userWeights.foreach(w => {Util.setToRandom(w.value); Util.project(w.value.asInstanceOf[DenseTensor1])})
    itemWeights.foreach(w => {Util.setToRandom(w.value); Util.project(w.value.asInstanceOf[DenseTensor1])})
  }

}