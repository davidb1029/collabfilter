package cc.iesl.belanger.CollabFilter

import collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: belanger
 * Date: 8/13/13
 * Time: 8:45 PM
 * To change this template use File | Settings | File Templates.
 */



trait TackbpMatrixFactorization {
   def linearPredict(row: Int, col: Int): Double
   def initialize() : Unit
   def columnSimilarity(col1: Int,col2: Int): Double
   def train(examples: Seq[Cell],useBPR: Boolean) : Unit

}


class TackbpMatrixFactorizationImpl(numRows: Int, numCols: Int,rank: Int, observedCoordinates: Seq[(Int,Int)]) extends TackbpMatrixFactorization {
  val matrixSpec = CollabMatrix(numRows,numCols,rank,mutable.HashSet[(Int,Int)]() ++= observedCoordinates)
  val model = new UVModel(matrixSpec)
  def linearPredict(row: Int, col: Int) = model.linearPredict(new Cell(row,col,1.0,true))
  def columnSimilarity(col1: Int,col2: Int): Double = model.columnSimilarity(col1,col2)
  def train(examples: Seq[Cell],useBPR: Boolean) : Unit = {
     MatrixCompletion.train(examples,matrixSpec,model,Seq(),true,useBPR, true)
  }

  def initialize() : Unit = {}

}
