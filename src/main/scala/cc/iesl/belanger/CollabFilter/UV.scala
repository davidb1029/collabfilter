package cc.iesl.belanger.CollabFilter

import cc.factorie._
import la._
import optimize._
import collection.mutable.{ArrayBuffer, HashSet}
import collection.mutable
import util.{DoubleSeq, DoubleAccumulator}
import io.Source
import cc.factorie.random

object MatrixCompletion {
  def main(args: Array[String]) {
    FactorizationOptions.parse(args)
    val rank = FactorizationOptions.rank.value.toInt
    val (cells,matrixSpec) = Util.loadMatrix(FactorizationOptions.dataFile.value,rank)
    val model = new UVModel(matrixSpec)
    val (trainCells,testCells) = random.shuffle(cells).split(0.8)
    val sampleNegCells = FactorizationOptions.addNegativeExamples.value.toBoolean
    val useBPR = FactorizationOptions.useBPR.value.toBoolean
    val useLogistic = FactorizationOptions.logisticLoss.value.toBoolean
    train(trainCells,matrixSpec,model,testCells, sampleNegCells,useBPR, useLogistic)

  }

  def train(trainCells: Seq[Cell], matrixSpec: CollabMatrix,model: UVModel, testCells: Seq[Cell] = Seq(), sampleNegCells: Boolean, useBPR: Boolean, useLogistic: Boolean) = {
    val decompSpec = if(useLogistic)  LogisticLoss else SquaredLoss

    val trainExamples = {
      if(sampleNegCells)
        makeExamplesWithNegatives(trainCells,matrixSpec,decompSpec,model,useBPR)     //this samples unobserved cells and assumes their value is zero
      else
        makeExamples(trainCells,matrixSpec,decompSpec,model)
    }

    val opt = new L2RegularizedConstantRateProjectedColumns(0.2/10000,0.1/10000)
    //val opt = new AdaGrad()
    model.initializeWeights
    var iter = 0
    def eval() = {
      evaluateCells(trainCells,model,decompSpec,"Train Iter " + iter)
      evaluateCells(testCells,model,decompSpec,"Test Iter " + iter)
      iter += 1
    }

    Trainer.onlineTrain(model.parameters, trainExamples, evaluate = eval,useParallelTrainer = false, maxIterations = FactorizationOptions.nIter.value.toInt,optimizer = opt /*,logEveryN=-1*/)(random)
  }


  def makeExamplesWithNegatives(cells: Seq[Cell],matrix: CollabMatrix,decomp: DecompSpec,model: FactorizationModel, useBPR: Boolean): Seq[Example] = {
    if(!useBPR){
      val negativeExample = new RandomNegativeExampleGenerator(matrix,decomp,0.0,model)
      cells.flatMap(c => Seq(new PositiveExample(c,matrix,decomp,model),negativeExample))
    } else{
      cells.flatMap(c => Seq(
        new PositiveExample(c,matrix,decomp,model),
        new BPRNegativeExampleGenerator(c,matrix,decomp,0.0,model)
      ))
    }
  }

  def makeExamples(cells: Seq[Cell],matrix: CollabMatrix,decomp: DecompSpec, model: FactorizationModel): Seq[Example] = {
    cells.map(c => new PositiveExample(c,matrix,decomp,model))
  }

  def evaluateCells(cells: Seq[Cell],model: FactorizationModel,decomp: DecompSpec,name: String): Double = {
    val totalLoss = cells.map(c => decomp.lossAndGradient(model.linearPredict(c),c.value)._1).sum
    val mse = totalLoss/cells.length
    println(name + " MSE: " + mse )
    mse
  }
}


//the expectation here is that negative cells correspond to unobserved cells, which we assume have a value of 0
class RandomNegativeExampleGenerator(val matrix: CollabMatrix,val decomp: DecompSpec,defaultValue: Double, thisModel: FactorizationModel) extends FactorizationExample{
  override def model = thisModel
  def cell: Cell = {
    var iCoord = -1
    var jCoord = -1
    do {
      iCoord = random.nextInt(matrix.numRows-1)
      jCoord = random.nextInt(matrix.numCols-1)
    }while(matrix.observedCells.contains((iCoord,jCoord)))
    new Cell(iCoord,jCoord,defaultValue,false)
  }
}

//the expectation here is that negative cells correspond to unobserved cells, which we assume have a value of 0
class BPRNegativeExampleGenerator(val observedCell: Cell,val matrix: CollabMatrix,val decomp: DecompSpec,defaultValue: Double, thisModel: FactorizationModel) extends FactorizationExample{
  override def model = thisModel
  val observedICoord = observedCell.iCoord
  val observedJCoord = observedCell.jCoord
  def cell: Cell = {
    var iCoord = -1
    var jCoord = -1

    do {
      iCoord = random.nextInt(matrix.numRows-1)
      jCoord = random.nextInt(matrix.numCols-1)
    }while(observedICoord == iCoord || observedJCoord == jCoord ||  matrix.observedCells.contains((iCoord,jCoord)))
    new Cell(iCoord,jCoord,defaultValue,false)
  }
}

class L2RegularizedConstantRateProjectedColumns(l2: Double = 0.1, rate: Double = 0.1) extends GradientOptimizer {

  def step(weights: WeightsSet, gradient: WeightsMap, value: Double): Unit = {
    weights += (gradient, rate)
    gradient.keys.foreach(w => w.value *= (1.0 - rate * l2))  //todo: might want to generalize this to have different l2s per family

    //only project the things that changed. This is problematic because it's projecting the biases too.
    gradient.keys.foreach(w => Util.project(w.value.asInstanceOf[DenseTensor1]))
  }
  def isConverged = false
  def reset(): Unit = { }
}

object SquaredLoss extends DecompSpec{
  def lossAndGradient(dotProd: Double,target: Double): (Double,Double) = cc.factorie.optimize.LinearObjectives.squaredUnivariate.valueAndGradient(dotProd,target)
  def predict(dotProd: Double): Double =  cc.factorie.optimize.LinearObjectives.squaredLossLinkFunction(dotProd)
}

object LogisticLoss extends DecompSpec{
  //todo: change this
  def lossAndGradient(dotProd: Double,target: Double): (Double,Double) = {
    assert(target.toInt == target)
    lossAndGradient(dotProd,target.toInt)
  }
  def lossAndGradient(dotProd: Double,target: Int): (Double,Double) = cc.factorie.optimize.LinearObjectives.logBinary.valueAndGradient(dotProd,target)
  def predict(dotProd: Double): Double =  cc.factorie.optimize.LinearObjectives.logisticLinkFunction(dotProd)
}

trait DecompSpec{
  def lossAndGradient(dotProd:Double,target: Double): (Double,Double)
  def predict(dotProd: Double): Double
}

class PositiveExample(val cell: Cell, val matrix: CollabMatrix, val decomp: DecompSpec, thisModel: FactorizationModel) extends FactorizationExample{override def model = thisModel}


trait FactorizationExample extends Example{
  def model: FactorizationModel
  def cell: Cell
  val matrix: CollabMatrix
  val decomp: DecompSpec
  def accumulateValueAndGradient(value: DoubleAccumulator, gradient: WeightsMapAccumulator): Unit =  model.accumulateValueAndGradient(cell,decomp,value,gradient)
}

final case class CollabMatrix(val numRows: Int,val numCols: Int,val rank: Int,val observedCells: HashSet[(Int,Int)])
final case class Cell(val iCoord: Int, val jCoord: Int, val value: Double, val isObserved: Boolean)

object FactorizationOptions extends cc.factorie.util.DefaultCmdOptions {
  val dataFile = new CmdOption("data", "", "FILE", "model file prefix")
  val rank =  new CmdOption("rank", "10", "INT", "model file prefix")
  val nIter =  new CmdOption("nIter", "25", "INT", "model file prefix")
  val addNegativeExamples = new CmdOption("addNegativeExamples","false","BOOL","whether to generate negative examples")
  val useBPR = new CmdOption("useBPR","false","BOOL","Whether to generate negative examples using BPR")
  val logisticLoss = new CmdOption("logistic","false","BOOL","Whether to use logistic loss")
}

object Util {


  //returns the cells, numRows, numColumns
  def loadMatrix(filename: String,rank: Int): (Seq[Cell],CollabMatrix) = {
    println("loading data from " + filename)
    val cells = ArrayBuffer[Cell]()
    val cellMap = HashSet[(Int,Int)]()
    val source = Source.fromFile(new java.io.File(filename))
    var rowMax = -1
    var colMax = -1
    for(line <- source.getLines()){
      val fields = line.split(" ")
      if(fields(0).toInt > rowMax) rowMax = fields(0).toInt
      if(fields(1).toInt > colMax) colMax = fields(1).toInt

      cells += new Cell(fields(0).toInt, fields(1).toInt, fields(2).toDouble, true)
    }
    val numCols = colMax + 1
    val numRows = rowMax + 1;
    (cells.toSeq,new CollabMatrix(numRows,numCols,rank,cellMap))
  }

  val rng = new java.util.Random(0)

  def setToRandom(t: Tensor1): Unit = {
    var i = 0
    while (i < t.length) {
      t(i) = rng.nextGaussian()*0.1
      i += 1
    }

  }

  def project(t: DenseTensor1): Unit = {
    if (t.twoNorm > 1)
      t.twoNormalize()
  }
}
